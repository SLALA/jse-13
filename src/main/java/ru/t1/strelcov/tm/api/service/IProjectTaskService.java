package ru.t1.strelcov.tm.api.service;

import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllTasksByProjectId(String projectId);

    Task bindTaskToProject(String taskId, String projectId);

    Task unbindTaskFromProject(String taskId);

    Project removeProjectById(String projectId);

}
