package ru.t1.strelcov.tm;

import ru.t1.strelcov.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
